let createNewUser=(name,surname)=>{
    return {
        firstName:name,
        lastName:surname,
        getLogin: function () {
           return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    },
        setFirstName: function(nameExample) {
            this.firstName=nameExample;
        },
        setLastName: function(surnameExample) {
            this.lastName=surnameExample;
        }
    }

    return newUser;
}
const newUser=createNewUser("Ivan",'Kravchenko');
console.log(newUser);
console.log(newUser.getLogin());